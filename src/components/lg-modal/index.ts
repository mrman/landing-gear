import { LitElement, html, css, unsafeCSS } from "lit"
import { customElement, property, query } from "lit/decorators.js"
import { classMap } from "lit/directives/class-map"
import { styleMap } from "lit/directives/style-map"

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

export enum LGModalVariant {
  TaglineAndLink = "tagline-and-link",
  BlurbAndLink = "blurb-and-link",
  SubscriptionForm = "subscription-form",
}

/**
 * Modal
 * See: https://blocks.wickedtemplates.com/modals
 *
 * @csspart container
 */
@customElement("lg-modal")
export class LGModal extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property()
  classes = {
    "component": true,
    "lg-modal": true,
    "items-center": true,
  };

  @property()
  styles = {};

  @property({ type: LGModalVariant })
  variant = LGModalVariant.TaglineAndLink;

  @property()
  header = "Top line smaller header";

  @property()
  class = "Top line smaller header";

  @property()
  title = "Large title for users to read";

  //////////////////////////////
  // Tagline And Link Variant //
  //////////////////////////////

  @property({ attribute: "link-title" })
  linkTitle = "Read more link title";
  @property({ attribute: "link-href" })
  linkHref = "#";
  @property({ attribute: "link-text" })
  linkText = "Read more link text";

  renderTaglineAndLinkVariant() {
    return html`
<div class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="w-full px-5 mx-auto border rounded-lg shadow-xl text-blueGray-500"
       aria-hidden="false"
       aria-describedby="modalDescription"
       role="dialog">

    <div class="flex justify-end px-6 py-4 ">
      <button class="p-1 transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none"
              type="button"
              aria-label="Close"
              @click="${this._handleCloseClick}"
              aria-hidden="true">
        <svg xmlns="http://www.w3.org/2000/svg"
             class="icon icon-tabler icon-tabler-circle-x"
             width="24"
             height="24"
             viewBox="0 0 24 24"
             stroke-width="1.5"
             stroke="currentColor"
             fill="none"
             stroke-linecap="round"
             stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <circle cx="12" cy="12" r="9"></circle>
          <path d="M10 10l4 4m0 -4l-4 4"></path>
        </svg>
      </button>
    </div>

    <div class="flex justify-center">
      <div class="flex flex-col w-7/8 sm:w-4/5 lg:w-3/5 mb-12">
        <h2 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.header}</h2>
        <h1 class="mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter text-black lg:w-1/2 sm:text-4xl title-font">${this.title}</h1>
        <a class="mx-auto text-sm font-semibold text-blue-600 hover:text-black" href="${this.linkHref}" title="${this.linkTitle}">${this.linkText}</a>
      </div>
    </div>

  </div>
</div>
`;
  }

  ////////////////////////////
  // Blurb and Link Variant //
  ////////////////////////////

  @property({ attribute: "long-blurb" })
  longBlurb = "A longer blurb that should convince the user to click on the action below.";

  @property({ attribute: "action-text" })
  actionText = "Action";

  renderBlurbAndLinkVariant() {
    return html`
<div class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="w-full px-5 mx-auto border rounded-lg shadow-xl text-blueGray-500"
       aria-hidden="false"
       aria-describedby="modalDescription"
       role="dialog">

    <div class="flex justify-end px-6 py-4 ">
      <button class="p-1 transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none"
              type="button"
              aria-label="Close"
              @click="${this._handleCloseClick}"
              aria-hidden="true">
        <svg xmlns="http://www.w3.org/2000/svg"
             class="icon icon-tabler icon-tabler-circle-x"
             width="24"
             height="24"
             viewBox="0 0 24 24"
             stroke-width="1.5"
             stroke="currentColor"
             fill="none"
             stroke-linecap="round"
             stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <circle cx="12" cy="12" r="9"></circle>
          <path d="M10 10l4 4m0 -4l-4 4"></path>
        </svg>
      </button>
    </div>

    <div class="flex justify-center">
      <div class="flex flex-col w-7/8 sm:w-4/5 lg:w-3/5 mb-12">
        <h2 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.header}</h2>
        <h1 class="mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter text-black lg:w-1/2 sm:text-4xl title-font">${this.title}</h1>
        <p class="mx-auto font-medium leading-relaxed text-blueGray-700">${this.longBlurb}</p>
        <div class="flex mt-4">
          <button class="w-auto px-4 py-2 my-2 font-medium text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blue-800"
                  @click="${this._handleActionClick}">
            ${this.actionText}
          </button>
        </div>
      </div>
    </div>

  </div>
</div>
`;
  }

  //////////////////
  // Form Variant //
  //////////////////

  @property({ attribute: "below-action-button-blurb" })
  belowActionButtonBlurb = "blurb below action button";

  @property({ attribute: "subscription-checkbox-text" })
  subscriptionCheckboxText = "Subscribe me";

  @query('input[name=name]', true)
  _nameInput!: HTMLInputElement;

  @query('input[name=email]', true)
  _emailInput!: HTMLInputElement;

  @query('input[name=subscribe]', true)
  _subscribeCheckbox!: HTMLInputElement;

  renderSubscriptionFormVariant() {
    return html`
<div class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="items-center px-5 lg:px-20">
  <div class="w-full px-5 mx-auto border rounded-lg shadow-xl lg:px-0 text-blueGray-500 lg:w-1/2"
       aria-hidden="false"
       aria-describedby="modalDescription"
       role="dialog">

    <!-- Close button -->
    <div class="flex items-center justify-end px-6 py-4 ">
      <button class="p-1 transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none" type="button" aria-label="Close" aria-hidden="true">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-circle-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <circle cx="12" cy="12" r="9"></circle>
          <path d="M10 10l4 4m0 -4l-4 4"></path>
        </svg>
      </button>
    </div>

    <!-- Headline above form -->
    <div class="flex flex-col w-full mx-auto text-left lg:px-20 lg:text-center">
      <h2 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.header}</h2>
      <h1 class="text-2xl font-semibold leading-none tracking-tighter text-black title-font">${this.title}</h1>
    </div>

    <div class="flex flex-col w-full mx-auto mb-8 lg:px-20 md:mt-0">

      <div class="relative mt-4">
        <label for="name" class="leading-7 text-blueGray-500">Name</label>
        <input class="w-full px-4 py-2 mt-2 text-black transition duration-500 ease-in-out transform border-transparent rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"
               type="name"
               id="name"
               name="name"
               placeholder="name">
      </div>

      <div class="relative mt-4">
        <label for="email" class="leading-7 text-blueGray-500">Email</label>
        <input class="w-full px-4 py-2 mt-2 text-black transition duration-500 ease-in-out transform border-transparent rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"
               type="email"
               id="email"
               name="email"
               placeholder="email">

      </div>

      <div class="flex my-6 mt-4">
        <label class="flex items-center">
          <input type="checkbox" class="form-checkbox" name="subscribe">
            <span class="ml-2 text-blueGray-500">${this.subscriptionCheckboxText}</span>
        </label>
      </div>

      <button class="w-full px-16 py-2 mr-2 font-medium text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blue-800"
              @click="${this._handleSubscriptionFormSubmit}">${this.actionText}</button>

      <p class="mx-auto mt-3 text-xs text-blueGray-500">${this.belowActionButtonBlurb}</p>

    </div>
  </div>
</div>
`;
  }

  ///////////////
  // Rendering //
  ///////////////

  render() {
    switch (this.variant) {
      case LGModalVariant.TaglineAndLink: return this.renderTaglineAndLinkVariant();
      case LGModalVariant.BlurbAndLink: return this.renderBlurbAndLinkVariant();
      case LGModalVariant.SubscriptionForm: return this.renderSubscriptionFormVariant();
      default:
        return this.renderTaglineAndLinkVariant();
    }
  }

  private _handleCloseClick() {
    const event = new Event("close-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

  private _handleActionClick() {
    const event = new Event("action-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

  private _handleSubscriptionFormSubmit() {
    const event = new CustomEvent("subscription-form-submitted", {
      bubbles: true,
      composed: true,
      detail: {
        email: this._emailInput.value.trim(),
        name: this._nameInput.value.trim(),
        subscribe: this._subscribeCheckbox.checked,
      },
    });
    this.dispatchEvent(event);
  }

}

declare global {
  interface HTMLElementTagNameMap {
    "lg-modal": LGModal
  }
}
