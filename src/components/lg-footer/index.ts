import {
  LitElement,
  TemplateResult,
  html,
  css,
  unsafeCSS,
} from "lit"
import { customElement, property, state } from "lit/decorators.js";

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

const DEFAULT_COLUMN_LIST_TITLE = "Links";

/**
 * Footer
 * See: https://blocks.wickedtemplates.com/footers
 *
 * @csspart container
 */
@customElement("lg-footer")
export class LGFooter extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
    color: var(--text-primary-color, 'black');
  }
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property({ attribute: "logo-text" })
  logoText: string = "logotext";

  @property({ attribute: "logo-src" })
  logoSrc: string = "";

  @property({ attribute: "logo-alt" })
  logoAlt: string = "logo image alt";

  @property({ attribute: "under-logo-text" })
  underLogoText = "Under Logo Text";

  @property({ attribute: "facebook-icon-href" })
  facebookIconHref?: string;

  @property({ attribute: "twitter-icon-href" })
  twitterIconHref?: string;

  @property({ attribute: "instagram-icon-href" })
  instagramIconHref?: string;

  @property({ attribute: "linkedin-icon-href" })
  linkedinIconHref?: string;

  @state()
  protected _listSlotItems: TemplateResult[] = [];

  ////////////////
  // Icon links //
  ////////////////

  protected iconLinksHTML() {
    const icons = [];

    if (this.facebookIconHref) {
      icons.push(html`
<a href="${this.facebookIconHref}" target="_blank" rel="noopener" class="text-blue-500 hover:text-black">
  <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
    <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
  </svg>
</a>
      `);
    }

    if (this.twitterIconHref) {
      icons.push(html`
<a href="${this.twitterIconHref}" target="_blank" rel="noopener" class="ml-3 text-blue-500 hover:text-black">
  <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
    <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
    </path>
  </svg>
</a>
  `);
    }

    if (this.instagramIconHref) {
      icons.push(html`
<a href="${this.instagramIconHref}" target="_blank" rel="noopener" class="ml-3 text-blue-500 hover:text-black">
  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
    <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
    <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
  </svg>
</a>
  `);
    }

    if (this.linkedinIconHref) {
      icons.push(html`
<a href="${this.linkedinIconHref}" target="_blank" rel="noopener" class="ml-3 text-blue-500 hover:text-black">
  <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="0" class="w-5 h-5" viewBox="0 0 24 24">
    <path stroke="none" d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z">
    </path>
    <circle cx="4" cy="4" r="2" stroke="none"></circle>
  </svg>
</a>
{`);
    }

    return icons;
  }

  ////////////////
  // List items //
  ////////////////

  protected _handleSlotChange() {
    if (!this.shadowRoot) { return; }

    // Build list of items to show
    const listItems: TemplateResult[] = [];

    // Extract list items from the slot element
    const linksSlot = this.shadowRoot.querySelector("slot[name=links]");
    if (!linksSlot) { return; }

    // Get the slot container
    const linksChildren = (linksSlot as any).assignedNodes({ flatten: true });
    if (!linksChildren || linksChildren.length === 0) { return; }

    // Retrieve list of ULs
    const topNode = linksChildren[0];
    const columns = topNode.querySelectorAll("link-column");

    // Process top <ul>s
    columns.forEach((col: HTMLElement) => {
      const title = col.getAttribute("title") || DEFAULT_COLUMN_LIST_TITLE;

      // Build links for each item in the list
      const links: TemplateResult[] = [];
      col
        .querySelectorAll("list-item")
        .forEach(elem => {
          const linkHref = elem.getAttribute("href") || "javascript:void(0)";
          links.push(html`
  <li>
    <a href="${linkHref}"
       class=" mr-1 text-sm text-blueGray-500 transition duration-500 ease-in-out transform rounded-sm focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">
      ${elem.textContent}
    </a>
  </li>
  `);
        });

      // Create the total listing
      listItems.push(html`
  <div class="link-column px-4">
    <h1 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${title}</h1>
    <nav class="mb-10 space-y-4 list-none">
      ${links}
    </nav>
  </div>
  `);
    });

    this._listSlotItems = listItems;
    this.requestUpdate(); // for some reason, lit doesn't update after
  }

  ///////////////
  // App Links //
  ///////////////

  @property({ attribute: "google-play-store-link-href" })
  googlePlayStoreLinkHref?: string;

  @property({ attribute: "google-play-store-link-text" })
  googlePlayStoreLinkText = "Get it on Google Play Store";

  @property({ attribute: "apple-app-store-link-href" })
  appleAppStoreLinkHref?: string;

  @property({ attribute: "apple-app-store-link-text" })
  appleAppStoreLinkText = "Get it on Apple App Store";

  @property({ attribute: "apps-column-title" })
  appsColumnTitle = "Apps";

  protected appLinksColumnHTML() {
    // If neither link is specified, don"t return anything
    if (!this.googlePlayStoreLinkHref && !this.appleAppStoreLinkHref) { return null; }

    const links = [];

    if (this.googlePlayStoreLinkHref) {
      links.push(html`
  <button class="inline-flex items-center px-5 py-3 text-black rounded-xl bg-blueGray-50 hover:bg-black hover:text-white focus:outline-none" style="/*! display: inline-block; */">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="w-6 h-6" viewBox="0 0 512 512">
      <path d="M99.617 8.057a50.191 50.191 0 00-38.815-6.713l230.932 230.933 74.846-74.846L99.617 8.057zM32.139 20.116c-6.441 8.563-10.148 19.077-10.148 30.199v411.358c0 11.123 3.708 21.636 10.148 30.199l235.877-235.877L32.139 20.116zM464.261 212.087l-67.266-37.637-81.544 81.544 81.548 81.548 67.273-37.64c16.117-9.03 25.738-25.442 25.738-43.908s-9.621-34.877-25.749-43.907zM291.733 279.711L60.815 510.629c3.786.891 7.639 1.371 11.492 1.371a50.275 50.275 0 0027.31-8.07l266.965-149.372-74.849-74.847z">
      </path>
    </svg>
    <span class="flex flex-col items-start ml-4 leading-none">
      <span class="font-medium title-font">${this.googlePlayStoreLinkText}</span>
    </span>
  </button>
      `);
    }

    if (this.appleAppStoreLinkHref) {
      links.push(html`
  <button class="inline-flex items-center px-5 py-3 text-black rounded-xl bg-blueGray-50 hover:bg-black hover:text-white focus:outline-none">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="w-6 h-6" viewBox="0 0 305 305">
      <path d="M40.74 112.12c-25.79 44.74-9.4 112.65 19.12 153.82C74.09 286.52 88.5 305 108.24 305c.37 0 .74 0 1.13-.02 9.27-.37 15.97-3.23 22.45-5.99 7.27-3.1 14.8-6.3 26.6-6.3 11.22 0 18.39 3.1 25.31 6.1 6.83 2.95 13.87 6 24.26 5.81 22.23-.41 35.88-20.35 47.92-37.94a168.18 168.18 0 0021-43l.09-.28a2.5 2.5 0 00-1.33-3.06l-.18-.08c-3.92-1.6-38.26-16.84-38.62-58.36-.34-33.74 25.76-51.6 31-54.84l.24-.15a2.5 2.5 0 00.7-3.51c-18-26.37-45.62-30.34-56.73-30.82a50.04 50.04 0 00-4.95-.24c-13.06 0-25.56 4.93-35.61 8.9-6.94 2.73-12.93 5.09-17.06 5.09-4.64 0-10.67-2.4-17.65-5.16-9.33-3.7-19.9-7.9-31.1-7.9l-.79.01c-26.03.38-50.62 15.27-64.18 38.86z">
      </path>
      <path d="M212.1 0c-15.76.64-34.67 10.35-45.97 23.58-9.6 11.13-19 29.68-16.52 48.38a2.5 2.5 0 002.29 2.17c1.06.08 2.15.12 3.23.12 15.41 0 32.04-8.52 43.4-22.25 11.94-14.5 17.99-33.1 16.16-49.77A2.52 2.52 0 00212.1 0z">
      </path>
    </svg>
    <span class="flex flex-col items-start ml-4 leading-none">
      <span class="font-medium title-font">${this.appleAppStoreLinkText}</span>
    </span>
  </button>
`);
}

    return html`
  <div class="w-full px-4 lg:w-1/3 md:w-3/4">
    <h1 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.appsColumnTitle}</h1>
    <nav class="mb-10">
      <div class="flex flex-col flex-wrap space-y-4" style="flex-direction: column;">
        ${links}
      </div>
    </nav>
  </div>`;
  }

  ///////////////
  // Rendering //
  ///////////////

  render(): TemplateResult {
    return html`
  <footer class="text-blueGray-700 transition duration-500 ease-in-out transform">
    <div class="flex flex-col flex-wrap p-8 md:items-center lg:items-start md:flex-row md:flex-no-wrap ">

      <div class="lhs-text min-w-1/3 text-center md:mx-0 md:text-left">
        <div class="pr-2 lg:pr-8 lg:px-6">

          <a href="/" class=" focus:outline-none">
            <div class="inline-flex items-center">
              <img src="${this.logoSrc}" alt="${this.logoAlt}"></img>
              <h2 class="block p-2 text-xl font-medium tracking-tighter text-black transition duration-500 ease-in-out transform cursor-pointer hover:text-blueGray-500 lg:text-x lg:mr-8">
                ${this.logoText}
              </h2>
            </div>
          </a>

          <p class="mb-10 text-xs font-semibold tracking-widest text-black title-font ">${this.underLogoText}</p>

          <span class="inline-flex justify-start sm:mb-12">
            ${this.iconLinksHTML()}
          </span>

        </div>
      </div>

      <div class="links-container flex  flex-wrap flex-grow justify-around mt-8 text-left md:pl-20 md:mt-0 ">
        <slot @slotchange="${this._handleSlotChange}" name="links" style="display:none;"></slot>
        ${this._listSlotItems}

        ${this.appLinksColumnHTML()}
      </div>

    </div>
  </footer>
</div>
      `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "lg-footer": LGFooter
  }
}
