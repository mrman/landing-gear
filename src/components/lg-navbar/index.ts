import {
  LitElement,
  html,
  css,
  unsafeCSS,
} from "lit"
import { customElement, property } from "lit/decorators.js";

import { classMap } from "lit/directives/class-map"
import { styleMap } from "lit/directives/style-map"

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

/**
 * Navbar
 * See: https://blocks.wickedtemplates.com/navigation
 *
 * @csspart container
 */
@customElement("lg-navbar")
export class LGNavbar extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
  }
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property()
  classes = {
    "component": true,
    "lg-navbar": true,
    "items-center": true,
  };

  @property()
  styles = {};

  @property({ attribute: "logo-text" })
  logoText: string = "logotext";

  @property({ attribute: "logo-image-src" })
  logoImageSrc: string = "vite-logo.svg";

  @property({ attribute: "logo-alt-text" })
  logoAltText: string = "logo image alt";

  @property({ attribute: "show-action-button", type: Boolean })
  showActionButton: boolean = true;

  @property({ attribute: "action-button-text" })
  actionButtonText: string = "Action";

  ///////////////
  // Rendering //
  ///////////////

  renderActionButton() {
    if (!this.showActionButton) { return null; }

    return html`
<button class="w-auto px-8 py-2 my-2 font-medium text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:b-gblue-700"
        @click="${this._handleActionClick}">
  ${this.actionButtonText}
</button>
`;

  }

  render() {
        return html`
<div class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="text-blueGray-700 rounded-lg">
    <div class="flex flex-col flex-wrap p-5 mx-auto md:items-center md:flex-row">

      <!-- Logo -->
      <a href="/" class="pr-2 lg:pr-8 lg:px-6 focus:outline-none">
        <div class="inline-flex items-center">
          <img class="max-h-8" src="${this.logoImageSrc}" alt="${this.logoAltText}"></img>
          <h2 class="block p-2 text-xl font-medium tracking-tighter text-black transition duration-500 ease-in-out transform cursor-pointer hover:text-blueGray-500 lg:text-x lg:mr-8">${this.logoText}</h2>
        </div>
      </a>

      <!-- Slot: "nav" -->
      <div class="nav-container flex-grow">
        <slot name="nav">
          <nav class="flex flex-wrap items-center lg:mr-auto">
            <ul class="items-center inline-block list-none lg:inline-flex">
              <li>
                <a href="#" class="px-4 py-1 mr-1 text-blueGray-500 transition duration-500 ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black ">Default</a>
              </li>
              <li>
                <a href="#" class="px-4 py-1 mr-1 text-blueGray-500 transition duration-500 ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black ">Default</a>
              </li>
              <li>
                <a href="#" class="px-4 py-1 mr-1 text-blueGray-500 transition duration-500 ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black ">Default</a>
              </li>
              <li>
                <a href="#" class="px-4 py-1 mr-1 text-blueGray-500 transition duration-500 ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black ">Default</a>
              </li>
            </ul>
          </nav>
        </slot>
      </div>

      ${this.renderActionButton()}
    </div>
  </div>
</div>
  `;
    }

  private _handleActionClick() {
    const event = new Event("action-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

}

declare global {
  interface HTMLElementTagNameMap {
    "lg-navbar": LGNavbar
  }
}
