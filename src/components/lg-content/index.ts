import { LitElement, html, css, unsafeCSS } from "lit";
import { customElement, property } from "lit/decorators.js";

import { classMap } from "lit/directives/class-map"
import { styleMap } from "lit/directives/style-map"

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

export enum LGContentVariant {
  Blurb = "blurb",
  FeatureChecks = "feature-checks",
  BlurbAndTwoStories = "blurb-and-two-stories",
  BlurbAndThreeFeaturettes = "blurb-and-three-featurettes",
  ProductTable = "product-table",
}

const CAMERA_LENS_SVG = html`
<svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" class="w-10 h-10" viewBox="0 0 24 24">
  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
  <circle cx="12" cy="12" r="9"></circle>
  <line x1="3.6" y1="15" x2="14.15" y2="15"></line>
  <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(72 12 12)"></line>
  <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(144 12 12)"></line>
  <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(216 12 12)"></line>
  <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(288 12 12)"></line>
</svg>
  `;

/**
 * Content
 * See: https://blocks.wickedtemplates.com/content
 *
 * @csspart container
 */
@customElement("lg-content")
export class LGContent extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
    min-width: 100%;
    min-height: 100%;
  }
  `;

  @property()
  classes = {
    "component": true,
    "lg-content": true,
    "items-center": true,
  };

  @property()
  styles = {};

  @property({ type: LGContentVariant })
  variant = LGContentVariant.Blurb;

  @property()
  title: string = "An interesting title that is sure to grab attention";

  @property()
  subtitle: string = "A more focused subtitle";

  @property({ attribute: "show-blurb-fragment", type: Boolean })
  showBlurbFragment = true;

  @property({ attribute: "blurb-text" })
  blurbText: string = "Long blurb goes here";

  ///////////
  // Blurb //
  ///////////

  renderBlurbVariant() {
        return html`
<section class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="container flex flex-col items-center px-5 py-8 mx-auto">
    <div class="flex flex-col w-full text-left ">
      <div class="w-full mx-auto lg:w-3/4">
        <h1 class="mx-auto mb-6 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">${this.title}</h1>
        <h2 class="mx-auto mb-4 text-xl font-semibold leading-none tracking-tighter title-font">${this.subtitle}</h2>
        <p class="mx-auto leading-relaxed text-blueGray-700 ">${this.blurbText}</p>
      </div>
    </div>
  </div>
</section>
  `;
  }

  ////////////////////
  // Feature Checks //
  ////////////////////

  @property({ attribute: "show-feature-checks-fragment", type: Boolean })
  showFeatureChecksFragment = true;

  @property({ attribute: "feature-check-1-text" })
  featureCheck1Text: string = "Text for the first feature check";

  @property({ attribute: "feature-check-2-text" })
  featureCheck2Text: string = "Text for the second feature check";

  @property({ attribute: "feature-check-3-text" })
  featureCheck3Text: string = "Text for the third feature check";

  renderFeatureChecksVariant() {
    const featureCheckFragment = [ this.featureCheck1Text, this.featureCheck2Text, this.featureCheck3Text ].map(text => {
      return html`
<p class="flex items-center mb-2 font-semibold text-blueGray-700 ">
  <span class="inline-flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 rounded-full">
    <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
      <path fill="none" d="M0 0h24v24H0z"></path>
      <path d="M10 15.172l9.192-9.193 1.415 1.414L10 18l-6.364-6.364 1.414-1.414z"></path>
    </svg>
  </span> ${text}
</p>
  `;
});

    let blurbFragment;
    if (this.showBlurbFragment) {
      blurbFragment = html`<p class="mx-auto mb-4 font-medium leading-relaxed text-blueGray-700 ">${this.blurbText}</p>`;
    }

    return html`
<section class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="container flex flex-col items-center px-5 py-8 mx-auto">
    <div class="flex flex-col w-full text-left ">
      <div class="w-full mx-auto lg:w-3/4">
        <h2 class="mx-auto mb-6 text-xl font-semibold leading-none tracking-tighter title-font">${this.title}</h2>
        ${blurbFragment}
        ${featureCheckFragment}
      </div>
    </div>
  </div>
</section>
  `;
  }

  ///////////////////////////
  // Blurb and Two Stories //
  ///////////////////////////

  @property({ attribute: "show-story-fragment", type: Boolean })
  showStoryFragment = true;

  @property({ attribute: "story-1-title" })
  story1Title: string = "Title for short headline";

  @property({ attribute: "story-1-blurb" })
  story1Blurb: string = "Content for first story. A nice long introduction here";

  @property({ attribute: "story-1-link-hover-text" })
  story1LinkHoverText: string = "#";

  @property({ attribute: "story-1-link-text" })
  story1LinkText: string = "#";

  @property({ attribute: "story-1-link-href" })
  story1LinkHref: string = "#";

  @property({ attribute: "story-2-title" })
  story2Title: string = "Title for short headline";

  @property({ attribute: "story-2-blurb" })
  story2Blurb: string = "Content for first story. A nice long introduction here";

  @property({ attribute: "story-2-link-hover-text" })
  story2LinkHoverText: string = "#";

  @property({ attribute: "story-2-link-text" })
  story2LinkText: string = "#";

  @property({ attribute: "story-2-link-href" })
  story2LinkHref: string = "#";

  renderBlurbAndTwoStoriesVariant() {

    let blurbFragment;
    if (this.showBlurbFragment) {
      blurbFragment = html`<p class="mx-auto font-medium leading-relaxed text-blueGray-700 ">${this.blurbText}</p>`;
    }

    return html`
<section class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="container flex flex-col items-center px-5 py-8 mx-auto">
    <div class="flex flex-col w-full mb-8 text-left ">
      <div class="w-full mx-auto lg:w-3/4">
        <h1 class="mx-auto mb-12 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">${this.title}</h1>
        <h2 class="mx-auto mb-4 text-xl font-semibold leading-none tracking-tighter title-font">${this.subtitle}</h2>
        ${blurbFragment}
      </div>
    </div>
  </div>

  <div class="container items-center px-5 mx-auto lg:px-28">
    <div class="flex flex-wrap justify-center">

      <div class="w-full lg:w-1/3">
        <div class="p-8">
          <h1 class="mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">
            ${this.story1Title}
          </h1>
          <p class="mx-auto font-medium leading-relaxed text-blueGray-700 ">${this.story1Blurb}</p>
          <a href="${this.story1LinkHref}"
             class="inline-flex items-center mt-4 font-semibold text-blue-600 lg:mb-0 hover:"
             title="${this.story1LinkHoverText}">
            ${this.story1LinkText}
          </a>
        </div>
      </div>

      <div class="w-full lg:w-1/3">
        <div class="p-8">
          <h1 class="mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">
            ${this.story2Title}
          </h1>
          <p class="mx-auto font-medium leading-relaxed text-blueGray-700 ">${this.story2Blurb}</p>
          <a href="${this.story2LinkHref}"
             class="inline-flex items-center mt-4 font-semibold text-blue-600 lg:mb-0 hover:"
             title="${this.story2LinkHoverText}">
            ${this.story2LinkText}
          </a>
        </div>
      </div>
    </div>

  </div>
</section>
`;
  }

  /////////////////
  // Featurettes //
  /////////////////

  @property({ attribute: "featurette-1-title" })
  featurette1Title?: string;

  @property({ attribute: "featurette-1-blurb" })
  featurette1Blurb?: string;

  @property({ attribute: "featurette-1-image-src" })
  featurette1ImageSrc?: string;

  @property({ attribute: "featurette-1-image-alt-text" })
  featurette1ImageAltText?: string;

  @property({ attribute: "featurette-2-title" })
  featurette2Title?: string;

  @property({ attribute: "featurette-2-blurb" })
  featurette2Blurb?: string;

  @property({ attribute: "featurette-2-image-src" })
  featurette2ImageSrc?: string;

  @property({ attribute: "featurette-2-image-alt-text" })
  featurette2ImageAltText?: string;

  @property({ attribute: "featurette-3-title" })
  featurette3Title?: string;

  @property({ attribute: "featurette-3-blurb" })
  featurette3Blurb?: string;

  @property({ attribute: "featurette-3-image-src" })
  featurette3ImageSrc?: string;

  @property({ attribute: "featurette-3-image-alt-text" })
  featurette3ImageAltText?: string;

  renderFeaturette(opts: {
    title?: string,
    blurb?: string,
    imageSrc?: string,
    imageAltText?: string
  }) {
    // If not options were provided, show nothing
    if (Object.values(opts).every(v => typeof v === "undefined")) { return null; }

    let imageFragment = CAMERA_LENS_SVG;
    if (opts.imageSrc) {
      imageFragment = html`<img src="${opts.imageSrc}" alt=${opts.imageAltText}/>`;
    }

    return html`
<div class="flex flex-col items-center pb-10 mx-auto mb-10 border-b border-blueGray-200 sm:flex-row lg:w-3/4">
  <div class="inline-flex items-center justify-center flex-shrink-0 w-20 h-20 rounded-full bg-blueGray-50 sm:mr-10">
    ${imageFragment}
  </div>

  <div class="flex-grow mt-6 text-center sm:text-left sm:mt-0">
    <h2 class="mb-8 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">${opts.title}</h2>
    <p class="font-medium leading-relaxed text-blueGray-700">${opts.blurb}</p>
  </div>
</div>
`;
  }

  renderBlurbAndThreeFeaturettesVariant() {
    const featurettesFragment = [
      this.renderFeaturette({
        title: this.featurette1Title,
        blurb: this.featurette1Blurb,
        imageSrc: this.featurette1ImageSrc,
        imageAltText: this.featurette1ImageAltText,
      }),

      this.renderFeaturette({
        title: this.featurette2Title,
        blurb: this.featurette2Blurb,
        imageSrc: this.featurette2ImageSrc,
        imageAltText: this.featurette2ImageAltText,
      }),

      this.renderFeaturette({
        title: this.featurette3Title,
        blurb: this.featurette3Blurb,
        imageSrc: this.featurette3ImageSrc,
        imageAltText: this.featurette3ImageAltText,
      }),
    ];

    return html`
<section class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="container flex flex-col items-center px-5 py-8 mx-auto">

    <div class="flex flex-col w-full text-left mb-6">
      <div class="w-full mx-auto lg:w-3/4">
        <h1 class="mx-auto mb-12 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">${this.title}</h1>
        <h2 class="mx-auto mb-4 text-xl font-semibold leading-none tracking-tighter title-font">${this.subtitle}</h2>
        <p class="mx-auto font-medium leading-relaxed text-blueGray-700 ">${this.blurbText}</p>
      </div>
    </div>

    ${featurettesFragment}

  </div>
</section>
    `;
  }

  ///////////////////
  // Product Table //
  ///////////////////

  @property({ attribute: "col-2-title" })
  col2Title?: string;

  @property({ attribute: "col-3-title" })
  col3Title?: string;

  @property({ attribute: "product-1-name" }) product1Name?: string;
  @property({ attribute: "product-1-col-2-text" }) product1Col2Text?: string;
  @property({ attribute: "product-1-col-3-text" }) product1Col3Text?: string;
  @property({ attribute: "product-1-buy-now-link-text" }) product1BuyNowLinkText?: string;

  @property({ attribute: "product-2-name" }) product2Name?: string;
  @property({ attribute: "product-2-col-2-text" }) product2Col2Text?: string;
  @property({ attribute: "product-2-col-3-text" }) product2Col3Text?: string;
  @property({ attribute: "product-2-buy-now-link-text" }) product2BuyNowLinkText?: string;

  @property({ attribute: "product-3-name" }) product3Name?: string;
  @property({ attribute: "product-3-col-2-text" }) product3Col2Text?: string;
  @property({ attribute: "product-3-col-3-text" }) product3Col3Text?: string;
  @property({ attribute: "product-3-buy-now-link-text" }) product3BuyNowLinkText?: string;

  @property({ attribute: "product-4-name" }) product4Name?: string;
  @property({ attribute: "product-4-col-2-text" }) product4Col2Text?: string;
  @property({ attribute: "product-4-col-3-text" }) product4Col3Text?: string;
  @property({ attribute: "product-4-buy-now-link-text" }) product4BuyNowLinkText?: string;

  private _handleBuyNowButtonClick(clickEvent: Event, name?: string) {
    const event = new CustomEvent("buy-now-button-clicked", {
      bubbles: true,
      composed: true,
      detail: { name, clickEvent },
    });
    this.dispatchEvent(event);
  }

  renderProductTableRow(opts: {
    name?: string;
    col2Text?: string;
    col3Text?: string;
    buyNowLinkText?: string;
  }) {
    // If not options were provided, show nothing
    if (Object.values(opts).every(v => typeof v === "undefined")) { return null; }

    return html`
<tr class="items-center">
  <td class="px-4 py-2 bg-blueGray-50">${opts.name}</td>
  <td class="px-4 py-2 bg-blueGray-50">${opts.col2Text}</td>
  <td class="px-4 py-2 bg-blueGray-50">${opts.col3Text}</td>
  <td class="px-4 py-2 text-lg bg-blueGray-50">
    <button role="button"
            @click="${(evt: Event) => this._handleBuyNowButtonClick(evt, opts.name)}"
            class="px-4 py-2 mx-auto font-medium text-blue-600 transition duration-500 ease-in-out transform bg-blue-100 rounded-lg hover:bg-blue-300 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">
      ${opts.buyNowLinkText || "Buy Now"}
    </button>
  </td>
</tr>
`;
  }

  renderProductTableVariant() {
    const tableRows = [
      this.renderProductTableRow({
        name: this.product1Name,
        col2Text: this.product1Col2Text,
        col3Text: this.product1Col3Text,
        buyNowLinkText: this.product1BuyNowLinkText,
      }),

      this.renderProductTableRow({
        name: this.product2Name,
        col2Text: this.product2Col2Text,
        col3Text: this.product2Col3Text,
        buyNowLinkText: this.product2BuyNowLinkText,
      }),

      this.renderProductTableRow({
        name: this.product3Name,
        col2Text: this.product3Col2Text,
        col3Text: this.product3Col3Text,
        buyNowLinkText: this.product3BuyNowLinkText,
      }),

      this.renderProductTableRow({
        name: this.product4Name,
        col2Text: this.product4Col2Text,
        col3Text: this.product4Col3Text,
        buyNowLinkText: this.product4BuyNowLinkText,
      }),
    ];

    return html`
<section class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="container px-5 py-24 mx-auto">
    <div class="w-full mx-auto overflow-auto lg:w-2/3">
      <h1 class="mx-auto mb-6 text-2xl font-semibold leading-none tracking-tighter lg:text-3xl title-font">${this.title}</h1>

      <table class="w-full text-left whitespace-no-wrap table-auto">
        <thead>
          <tr>
            <th class="px-4 py-2 text-sm font-medium tracking-widest text-blueGray-500 title-font">Name</th>
            <th class="px-4 py-2 text-sm font-medium tracking-widest text-blueGray-500 title-font">${this.col2Title}</th>
            <th class="px-4 py-2 text-sm font-medium tracking-widest text-blueGray-500 title-font">${this.col3Title}</th>
            <th class="px-4 py-2 text-sm font-medium tracking-widest text-blueGray-500 title-font">Purchase</th>
          </tr>
        </thead>

        <tbody>
          ${tableRows}
        </tbody>
      </table>

    </div>
  </div>
</section>
    `;
}

  ///////////////
  // Rendering //
  ///////////////

  render() {
    switch (this.variant) {
      case LGContentVariant.Blurb: return this.renderBlurbVariant();
      case LGContentVariant.FeatureChecks: return this.renderFeatureChecksVariant();
      case LGContentVariant.BlurbAndTwoStories: return this.renderBlurbAndTwoStoriesVariant();
      case LGContentVariant.BlurbAndThreeFeaturettes: return this.renderBlurbAndThreeFeaturettesVariant();
      case LGContentVariant.ProductTable: return this.renderProductTableVariant();
      default:
        return this.renderBlurbVariant();
    }
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "lg-content": LGContent
  }
}
