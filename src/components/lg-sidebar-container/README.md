# `<lg-sidebar-container>`

The `<lg-sidebar-container>` component can be used as a container to show content along with a sidebar

# Image

TODO: IMAGE

# Example

```html
<lg-sidebar-container>

  <!-- Content -->
  <div slot="content" style="min-width: 100%; min-height: 100%; display: flex; flex-direction: column;align-items: center; justify-content: center;">
    <h1>Content</h1>
    <h2>Goes here</h2>
  </div>

  <!-- Nav -->
  <div slot="nav">
    <nav class="lhs-nav">
      <ul>
        <li class="item active">
          <a href="#">Default</a>
        </li>
        <li class="item">
          <a href="#">Default</a>
        </li>
        <li class="item">
          <a href="#">Default</a>
        </li>
        <li class="item">
          <a href="#">Default</a>
        </li>
      </ul>
    </nav>
  </div>

</lg-sidebar-container>
```
