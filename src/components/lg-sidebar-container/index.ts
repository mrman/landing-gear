import { LitElement, html, css, unsafeCSS } from "lit"
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map"
import { styleMap } from "lit/directives/style-map"

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

export enum UserFragmentPosition {
  Top = "top",
  Bottom = "bottom",
}

/**
 * Sidebar Container
 * See: https://blocks.wickedtemplates.com/sidebars
 *
 * @csspart container
 */
@customElement("lg-sidebar-container")
export class LGSidebarContainer extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
  }
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property()
  classes = {
    "component": true,
    "lg-sidebar-container": true,
    "flex-col": true,
    "w-full": true,
    "md:flex": true,
    "md:flex-row": true,
    "md:min-h-screen": true,
    "bg-blueGray-50": true,
  };

  @property({ type: Boolean })
  styles = {};

  @property({ type: Boolean })
  expanded = true;

  @property()
  title = "Your App";

  @property({ attribute: "show-logo-fragment", type: Boolean })
  showLogoFragment = true;

  @property({ attribute: "logo-image-src" })
  logoImageSrc = "vite-logo.svg";

  @property({ attribute: "logo-alt-text" })
  logoAltText = "Logo";

  @property({ attribute: "show-user-fragment", type: Boolean })
  showUserFragment = false;

  @property({ attribute: "user-fragment-position", type: UserFragmentPosition })
  userFragmentPosition: UserFragmentPosition = UserFragmentPosition.Top;

  @property({ attribute: "user-image-src" })
  userImageSrc = "dummy-image.300x300.png";

  @property({ attribute: "user-image-alt-text" })
  userImageAltText = "User image";

  @property()
  username = "User Name";

  @property({ attribute: "user-subtitle" })
  userSubtitle = "User Subtitle";

  ///////////////
  // Rendering //
  ///////////////

  renderLogoFragment() {
    if (!this.showLogoFragment) { return null; }
    return html`<img class="max-h-8" src="${this.logoImageSrc}" alt="${this.logoAltText}"></img>`;
  }

  renderExpandOrHideIcon() {
    if (!this.expanded) {
      return html`
<svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
<path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z" clip-rule="evenodd"></path>
</svg>
`;
    }
      return html`
<svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
          <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
</svg>
`;
  }

  renderUserFragmentTop() {
    if (!this.showUserFragment) { return null; }
    if (this.userFragmentPosition !== UserFragmentPosition.Top) { return null; }

    return html`
<div class="flex flex-col items-center flex-shrink-0 pb-4 bg-blueGray-50">
  <img class="inline-block object-cover object-center w-20 h-20 mb-8 bg-blueGray-100 rounded-full"
       src="${this.userImageSrc}"
       alt="${this.userImageAltText}">
    <h2 class="text-sm font-medium tracking-widest text-black title-font">${this.username}</h2>
    <p class="text-blueGray-500">${this.userSubtitle}</p>
</div>
`;

  }

  renderUserFragmentBottom() {
    if (!this.showUserFragment) { return null; }
    if (this.userFragmentPosition !== UserFragmentPosition.Bottom) { return null; }

    return html`
<div class="flex items-center w-full p-4 rounded-lg ">
  <img class="object-cover w-10 h-10 mr-4 border border-white border-solid rounded-full"
       src="${this.userImageSrc}"
       alt="${this.userImageAltText}" >
    <div>
      <h2 class="text-sm font-medium tracking-widest text-black title-font">${this.username}</h2>
      <p class="text-blueGray-500">${this.userSubtitle}</p>
    </div>
</div>
`;

  }

  render() {
    return html`
<div class="${classMap(this.classes)}" style="${styleMap(this.styles)}">
  <div class="flex flex-col flex-shrink-0 w-full bg-white shadow-xl text-blueGray-700 md:w-64">

    <!-- LHS -->
    <div class="flex flex-row items-center justify-between flex-shrink-0 py-4">
      <a href="/" class="px-8 focus:outline-none">

        <!-- logo -->
        <div class="inline-flex items-center">
          ${this.renderLogoFragment()}

          <!-- title -->
          <h2 class="block p-2 text-xl font-medium tracking-tighter text-black transition duration-500 ease-in-out transform cursor-pointer hover:text-blueGray-500 lg:text-x lg:mr-8">${this.title}</h2>
        </div>
      </a>

      <!-- Close button -->
      <button class="rounded-lg focus:outline-none focus:shadow-outline p-2" @click="${this._handleClose}">
        <!-- expand/hide button -->
        ${this.renderExpandOrHideIcon()}
        <!-- Expand button -->
      </button>

    </div>

    <!-- User segment when at top -->
    ${this.renderUserFragmentTop()}

    <!-- Slot: "nav" -->
    <div class="nav-container flex-grow">
      <slot name="nav">
        <nav class="pb-4 pr-4 md:block md:pb-0 md:overflow-y-auto">
          <ul>
            <li>
              <a class="block px-4 py-2 mt-2 transition duration-500 ease-in-out transform border-l-4 border-blue-600 text-blueGray-500 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black" href="#">Default</a>
            </li>
            <li>
              <a class="block px-4 py-2 mt-2 transition duration-500 ease-in-out transform border-l-4 border-white text-blueGray-500 hover:border-blue-600 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black" href="#">Default</a>
            </li>
            <li>
              <a class="block px-4 py-2 mt-2 transition duration-500 ease-in-out transform border-l-4 border-white text-blueGray-500 hover:border-blue-600 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black" href="#">Default</a>
            </li>
            <li>
              <a class="block px-4 py-2 mt-2 transition duration-500 ease-in-out transform border-l-4 border-white text-blueGray-500 hover:border-blue-600 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:text-black" href="#">Default</a>
            </li>
          </ul>
        </nav>
      </slot>
    </div>

    ${this.renderUserFragmentBottom()}

  </div>

  <!-- Slot: "content" -->
  <slot name="content" style="display: flex; flex-direction: column; min-width: 100%"></slot>
</div>
  `;
  }

  private _handleClose() {
    const event = new Event("close-clicked", {bubbles: true, composed: true});
    this.dispatchEvent(event);
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "lg-sidebar-container": LGSidebarContainer
  }
}
