import { LitElement, html, css, unsafeCSS } from "lit";
import { customElement, property } from "lit/decorators.js";

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

export enum LGAlertVariant {
  Error = "error",
  Info = "info",
  Success = "success",
  Warning = "warning",
}

export enum LGAlertSizeVariant {
  Thin = "thin",
  Thick = "thick",
}

/**
 * Alert
 * See: https://blocks.wickedtemplates.com/alerts
 *
 * @csspart container
 */
@customElement("lg-alert")
export class LGAlert extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
  }

  .alert.success .title { color: #059669; }
  .alert.success .close-button { color: #059669; }

  .alert.warning .title { color: #d97706; }
  .alert.warning .close-button { color: #d97706; }

  .alert.info .title { color: #2563eb; }
  .alert.info .close-button { color: #2563eb; }

  .alert.error .title { color: #dc2626; }
  .alert.error .close-button { color: #dc2626; }
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property({ type: LGAlertVariant })
  variant = LGAlertVariant.Info;

  @property({ attribute: "size-variant", type: LGAlertSizeVariant })
  sizeVariant = LGAlertSizeVariant.Thick;

  @property()
  title = "Something happened";

  @property()
  description = "Description of what has happened";

  @property({ type: Boolean })
  closable = false;

  ////////////////////
  // Variant - Info //
  ////////////////////

  protected variantIconHTML() {
    switch (this.variant) {
      case LGAlertVariant.Success:
        return html`
        <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 icon icon-tabler icon-tabler-circle-check" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <circle cx="12" cy="12" r="9"></circle>
          <path d="M9 12l2 2l4 -4"></path>
        </svg>
`;

      default:
    return html`
<svg xmlns="http://www.w3.org/2000/svg" class="mr-1 icon icon-tabler icon-tabler-alert-triangle" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
  <circle cx="12" cy="12" r="9"></circle>
  <line x1="12" y1="8" x2="12.01" y2="8"></line>
  <polyline points="11 12 12 12 12 16 13 16"></polyline>
</svg>
`;
    }
  }

  ////////////////////
  // Action buttons //
  ////////////////////

  @property({ attribute: "show-action-1", type: Boolean })
  showAction1 = false;

  @property({ attribute: "action-1-text" })
  action1Text = "Action 1";

  @property({ attribute: "action-1-href" })
  action1Href = "javascript:void(0)";

  @property({ attribute: "show-action-2", type: Boolean })
  showAction2 = false;

  @property({ attribute: "action-2-text" })
  action2Text = "Action 2";

  @property({ attribute: "action-2-href" })
  action2Href = "javascript:void(0)";

  protected actionButtonHTML() {
    // If neither action 1 nor 2 are specified, don"t show the action button
    if (!this.showAction1 && !this.showAction2) { return null; }

    const actionButtons = [];

    if (this.showAction1) {
      actionButtons.push(html`
        <a href="${this.action1Href}"
           @click="${this._handleAction1Click}"
           class="w-auto font-bold text-blue-500 transition duration-500 ease-in-out transform focus:shadow-outline focus:outline-none hover:text-black">
          ${this.action1Text}
        </a>
`);
    }

    if (this.showAction2) {
      actionButtons.push(html`
        <a href="${this.action2Href}"
           @click="${this._handleAction2Click}"
           class="w-auto ml-6 text-blueGray-500 transition duration-500 ease-in-out transform hover:text-black focus:shadow-outline focus:outline-none ">
          ${this.action2Text}
        </a>
`);
    }

    return html`
        <div class="flex items-center w-full p-6">
          ${actionButtons}
        </div>
`;

  }

  /////////////
  // Content //
  /////////////

  protected closeIconHTML() {
        return html`
      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-circle-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
        <circle cx="12" cy="12" r="9"></circle>
        <path d="M10 10l4 4m0 -4l-4 4"></path>
      </svg>
`;
  }

  protected contentHTML() {
    if (this.sizeVariant === LGAlertSizeVariant.Thin) {
      return html`
   <div class="flex flex-grow rounded-lg space-between">
      <div class="title inline-flex items-center w-full">
        ${this.variantIconHTML()}
        <strong>${this.title}</strong>
      </div>

    <button type="button"
            aria-label="Close"
            aria-hidden="true"
            @click="${this._handleCloseClick}"
            class="close-button transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none">
      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-circle-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
        <circle cx="12" cy="12" r="9"></circle>
        <path d="M10 10l4 4m0 -4l-4 4"></path>
      </svg>
    </button>

    </div>
`;
    }

    return html`
    <div class="flex-grow p-6 py-2 rounded-lg">
      <div class="title inline-flex items-center w-full ">
        ${this.variantIconHTML()}
        <strong>${this.title}</strong>
      </div>
      <p class="description my-4 text-sm font-semibold tracking-wide">${this.description}</p>
    </div>
`;
  }

  //////////////////////////
  // Outside close button //
  //////////////////////////

  protected outsideCloseButton() {
    if (this.sizeVariant === LGAlertSizeVariant.Thin) { return null; }

    return html`
    <button type="button"
            aria-label="Close"
            aria-hidden="true"
            @click="${this._handleCloseClick}"
            class="close-button float-right transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none">
      ${this.closeIconHTML()}
    </button>
`;
  }

  ///////////////
  // Rendering //
  ///////////////

  render() {
        return html`
<div class="alert ${this.variant} container items-center px-5 lg:px-20">
  <div class="p-2 mx-auto my-6 bg-white border rounded-lg shadow-xl lg:w-3/4">

    <!-- outside close button -->
    ${this.outsideCloseButton()}

    <!-- Content -->
    ${this.contentHTML()}

    <!-- Action buttons -->
    ${this.actionButtonHTML()}
  </div>
</div>
  `;
    }

  private _handleCloseClick() {
    const event = new Event("close-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

  private _handleAction1Click() {
    const event = new Event("action-1-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

  private _handleAction2Click() {
    const event = new Event("action-2-clicked", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "lg-alert": LGAlert
  }
}
