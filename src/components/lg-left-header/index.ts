import { LitElement, html, css, unsafeCSS } from "lit"
import { customElement, property, query } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map"

import tailwindStyles from "bundle-text:./tailwind-styles.generated.css";

export enum LGLeftHeaderVariant {
  ActionButtonAndDescription = "action-button-and-description",
  EmailAndActionButton = "email-and-action-button",
  FourLogos = "four-logos",
  ThreeItemChecklist = "three-item-checklist",
  TwoCard = "two-card",
}

/**
 * Left Header
 * See: https://blocks.wickedtemplates.com/left-headers
 *
 *
 * @cssprop --action-button-bg-color - The background color of the action button
 * @cssprop --action-button-fg-color - The foreground (text) color of the action button
 *
 * @csspart container
 */
@customElement("lg-left-header")
export class LGLeftHeader extends LitElement {
  static styles = css`
  /* Tailwind styles */
  ${unsafeCSS(tailwindStyles)}

  /* Component styling */
  :host {
    display: block;
  }

  button.action {
    background-color: var(--action-button-bg-color, rgba(37, 99, 235, var(--tw-bg-opacity)));
    color: var(--action-button-fg-color, rgba(255, 255, 255, var(--tw-text-opacity)));
  }

  input.email {
    background-color: #E2E8F0;
  }
  `;

  ///////////////////////
  // Shared properties //
  ///////////////////////

  @property()
  classes = {
    "text-blueGray-700": true,
    "component": true,
    "lg-left-header": true,
    "items-center": true,
  };

  @property({ type: LGLeftHeaderVariant })
  variant = LGLeftHeaderVariant.ActionButtonAndDescription;

  @property()
  title = "Medium length display headline";

  @property()
  tagline = "Your tagline";

  @property({ attribute: "main-text" })
  mainText = "Deploy your mvp in minutes, not days. WT offers you a a wide selection swapable sections for your landing page.";

  @property({ attribute: "action-button-text" })
  actionButtonText = "Action";

  @property({ attribute: "action-description" })
  actionDescription = "It will take you to candy shop.";

  @property({ attribute: "action-description-href" })
  actionDescriptionHref = "#";

  @property({ attribute: "action-description-link-text" })
  actionDescriptionLinkText = "Read more about it »";

  @property({ attribute: "action-text" })
  actionText = "It will take you to candy shop.";

  @property({ attribute: "action-description-link-text-tooltip" })
  actionDescriptionLinkTextTooltip = "Read more";

  @property({ attribute: "rhs-image-alt" })
  rhsImageAlt = "Hero image";

  @property({ attribute: "rhs-image-src" })
  rhsImageSrc = "dummy-image-720x600-gray.png";

  ////////////////
  // Four Logos //
  ////////////////

  @property({ attribute: "logo-1-alt" })
  logo1Alt = "";
  @property({ attribute: "logo-1-width", type: Number })
  logo1Width = 42;
  @property({ attribute: "logo-1-height", type: Number })
  logo1Height = 42;
  @property({ attribute: "logo-1-src" })
  logo1Src = "";

  @property({ attribute: "logo-2-alt" })
  logo2Alt = "";
  @property({ attribute: "logo-2-width", type: Number })
  logo2Width = 42;
  @property({ attribute: "logo-2-height", type: Number })
  logo2Height = 42;
  @property({ attribute: "logo-2-src" })
  logo2Src = "";

  @property({ attribute: "logo-3-alt" })
  logo3Alt = "";
  @property({ attribute: "logo-3-width", type: Number })
  logo3Width = 42;
  @property({ attribute: "logo-3-height", type: Number })
  logo3Height = 42;
  @property({ attribute: "logo-3-src" })
  logo3Src = "";

  @property({ attribute: "logo-4-alt" })
  logo4Alt = "";
  @property({ attribute: "logo-4-width", type: Number })
  logo4Width = 42;
  @property({ attribute: "logo-4-height", type: Number })
  logo4Height = 42;
  @property({ attribute: "logo-4-src" })
  logo4Src = "";

  protected fourLogosHTML() {
        return html`
<section class="${classMap(this.classes)}">
  <div class="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row lg:px-28">
    <div class="flex flex-col items-start w-full pt-0 mb-16 text-left lg:flex-grow md:w-3/4 xl:mr-10 md:pr-12 md:mb-0 ">
      <h1 class="mb-8 text-2xl font-bold tracking-tighter text-left text-black lg:text-2xl title-font">${this.title}</h1>
      <p class="mb-8 leading-relaxed text-left text-blueGray-700">${this.mainText}</p>
      <div class="flex flex-wrap w-full mt-2 -mx-4 text-left ">
        <div class="w-1/4 p-4 mt-4 sm:w-1/4">
          <img width="${this.logo1Width}" height="${this.logo1Height}" alt="${this.logo1Alt}" src="${this.logo1Src}"/>
        </div>
        <div class="w-1/4 p-4 mt-4 sm:w-1/4">
          <img width="${this.logo2Width}" height="${this.logo2Height}" alt="${this.logo2Alt}" src="${this.logo2Src}"/>
        </div>
        <div class="w-1/4 p-4 mt-4 sm:w-1/4">
          <img width="${this.logo3Width}" height="${this.logo3Height}" alt="${this.logo3Alt}" src="${this.logo3Src}"/>
        </div>
        <div class="w-1/4 p-4 mt-4 sm:w-1/4">
          <img width="${this.logo4Width}" height="${this.logo4Height}" alt="${this.logo4Alt}" src="${this.logo4Src}"/>
        </div>
      </div>
    </div>
    <div class="w-5/6 lg:max-w-lg lg:w-full md:w-3/4">
      <img class="object-cover object-center rounded-lg " alt="${this.rhsImageAlt}" src="${this.rhsImageSrc}">
    </div>
  </div>
</section>
    `;
  }

  //////////////////////////
  // Three Item Checklist //
  //////////////////////////

  @property({ attribute: "checklist-item-1" })
  checklistItem1 = "";
  @property({ attribute: "checklist-item-2" })
  checklistItem2 = "";
  @property({ attribute: "checklist-item-3" })
  checklistItem3 = "";

  protected threeItemChecklistHTML() {
        return html`
<section class="${classMap(this.classes)}">
  <div class="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row lg:px-28">
    <div class="flex flex-col items-start w-full pt-0 mb-16 text-left lg:flex-grow md:w-3/4 xl:mr-20 md:pr-24 md:mb-0 ">
      <h1 class="mb-8 text-2xl font-bold tracking-tighter text-left text-black lg:text-2xl title-font">${this.title}</h1>
      <p class="mb-8 leading-relaxed text-left text-blueGray-600">${this.mainText}</p>

      <p class="flex items-center mb-2 text-blueGray-600 "><span class="inline-flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 rounded-full">
          <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="none" d="M0 0h24v24H0z"></path>
            <path d="M10 15.172l9.192-9.193 1.415 1.414L10 18l-6.364-6.364 1.414-1.414z"></path>
          </svg>
        </span>
        ${this.checklistItem1}
      </p>

      <p class="flex items-center mb-2 text-blueGray-600 ">
        <span class="inline-flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 rounded-full">
          <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="none" d="M0 0h24v24H0z"></path>
            <path d="M10 15.172l9.192-9.193 1.415 1.414L10 18l-6.364-6.364 1.414-1.414z"></path>
          </svg>
        </span>
        ${this.checklistItem2}
      </p>

      <p class="flex items-center mb-6 text-blueGray-600">
        <span class="inline-flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 rounded-full">
          <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="none" d="M0 0h24v24H0z"></path>
            <path d="M10 15.172l9.192-9.193 1.415 1.414L10 18l-6.364-6.364 1.414-1.414z"></path>
          </svg>
        </span>
        ${this.checklistItem3}
      </p>

    </div>

    <div class="w-full lg:w-5/6 lg:max-w-lg md:w-3/4">
      <img class="object-cover object-center rounded-lg " alt="${this.rhsImageAlt}" src="${this.rhsImageSrc}">
    </div>
  </div>
</section>
  `;
  }

  /////////////////////////////
  // Email and Action Button //
  /////////////////////////////

  @property({ attribute: "below-email-blurb" })
  belowEmailBlurb = "I got 99 problems and blocks ain't one";

  @query('input[name=email]', true) _emailInput!: HTMLInputElement;

  protected emailAndActionButtonHTML() {
    return html`
<section class="${classMap(this.classes)}">
  <div class="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row lg:px-28">
    <div class="flex flex-col items-start w-full pt-0 mb-16 text-left lg:flex-grow md:w-3/4 xl:mr-20 md:pr-24 md:mb-0 ">
      <h1 class="mb-8 text-2xl font-black tracking-tighter text-black md:text-5xl title-font">${this.title}</h1>
      <p class="mb-8 leading-relaxed text-left text-blueGray-600 ">${this.mainText}</p>

      <div class="flex flex-col w-full gap-2 md:justify-start md:flex-row">
        <input class="email flex-grow w-full px-4 py-2 mb-4 text-black transition duration-650 ease-in-out transform rounded-lg lg:w-auto bg-blueGray-200 focus:outline-none focus:border-purple-500 sm:mb-0 focus:bg-white focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" placeholder="Your Email" type="email">
          <button @click="${this._handleActionClick}"
                  class="action flex items-center px-6 py-2 mt-auto font-semibold text-white transition duration-500 ease-in-out transform bg-blue-600 rounded-lg hover:bg-blue-700 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">
            ${this.actionButtonText}
          </button>
      </div>
      <p class="w-full mt-2 mb-8 text-sm text-left text-blueGray-600">${this.belowEmailBlurb}</p>
    </div>

    <div class="w-full lg:w-5/6 lg:max-w-lg md:w-3/4">
      <img class="object-cover object-center rounded-lg " alt="${this.rhsImageAlt}" src="${this.rhsImageSrc}">
    </div>

  </div>
</section>
      `;
  }

  //////////////
  // Two Card //
  //////////////

  @property({ attribute: "card-1-title" })
  card1Title = "First card title";
  @property({ attribute: "card-1-description" })
  card1Description = "This is the description on the second card";
  @property({ attribute: "card-1-image-alt" })
  card1ImageAlt = "First image";
  @property({ attribute: "card-1-image-width", type: Number })
  card1ImageWidth = 42;
  @property({ attribute: "card-1-image-height", type: Number })
  card1ImageHeight = 42;
  @property({ attribute: "card-1-image-src" })
  card1ImageSrc = "";
  @property({ attribute: "card-1-link-text" })
  card1LinkText = "Learn more";
  @property({ attribute: "card-1-link-href" })
  card1LinkHref = "#";
  @property({ attribute: "card-1-link-tooltip" })
  card1LinkTooltip = "";

  @property({ attribute: "card-2-title" })
  card2Title = "Second card title";
  @property({ attribute: "card-2-description" })
  card2Description = "This is the description on the second card";
  @property({ attribute: "card-2-image-alt" })
  card2ImageAlt = "Second image";
  @property({ attribute: "card-2-image-width", type: Number })
  card2ImageWidth = 42;
  @property({ attribute: "card-2-image-height", type: Number })
  card2ImageHeight = 42;
  @property({ attribute: "card-2-image-src" })
  card2ImageSrc = "";
  @property({ attribute: "card-2-link-text" })
  card2LinkText = "Learn More";
  @property({ attribute: "card-2-link-href" })
  card2LinkHref = "#";
  @property({ attribute: "card-2-link-tooltip" })
  card2LinkTooltip = "Learn more";

  protected twoCardHTML() {
        return html`
<section class="${classMap(this.classes)}">
  <div class="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row lg:px-28">
    <div class="flex flex-col w-full pt-0 mb-16 text-left lg:flex-grow md:w-3/4 xl:mr-16 md:pr-16 md:items-start md:mb-0 ">
      <h2 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.tagline}</h2>
      <h1 class="mb-8 text-2xl font-bold leading-snug tracking-tighter text-left text-black lg:text-5xl title-font">${this.title}</h1>

      <!-- columns -->
      <div class="flex flex-wrap -mx-4 -mt-4 -mb-10 sm:-m-4 ">

        <!-- first card -->
        <div class="flex flex-col items-start p-4 mb-6 text-left md:w-3/4 md:mb-0">
          <div class="inline-flex items-center justify-center flex-shrink-0 w-12 h-12 mb-5 text-black rounded-full bg-blueGray-50">
            <img width="${this.card1ImageWidth}" height="${this.card1ImageHeight}" alt="${this.card1ImageAlt}" src="${this.card1ImageSrc}"/>
          </div>
          <div class="flex-grow">
            <h2 class="mb-3 text-lg font-medium tracking-tighter text-blueGray-700 title-font">${this.card1Title}</h2>
            <p class="leading-relaxed">${this.card1Description}</p>
            <a href="${this.card1LinkHref}" title="${this.card1LinkTooltip}" class="inline-flex items-center font-semibold text-blue-600 md:mb-2 lg:mb-0 hover:text-black">
              ${this.card1LinkText}
              <svg class="w-4 h-4 ml-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20" fill="currentColor">
                <path fill="none" d="M0 0h24v24H0z"></path>
                <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"></path>
              </svg>
            </a>
          </div>
        </div>

        <!-- second card -->
        <div class="flex flex-col items-start p-4 mb-6 text-left md:w-3/4 md:mb-0">
          <div class="inline-flex items-center justify-center flex-shrink-0 w-12 h-12 mb-5 text-black rounded-full bg-blueGray-50">
            <img width="${this.card2ImageWidth}" height="${this.card2ImageHeight}" alt="${this.card2ImageAlt}" src="${this.card2ImageSrc}"/>
          </div>
          <div class="flex-grow">
            <h2 class="mb-3 text-lg font-medium tracking-tighter text-blueGray-700 title-font">${this.card2Title}</h2>
            <p class="leading-relaxed">${this.card2Description}</p>
            <a href="${this.card2LinkHref}" title="${this.card2LinkTooltip}" class="inline-flex items-center font-semibold text-blue-600 md:mb-2 lg:mb-0 hover:text-black ">
              ${this.card2LinkText}
              <svg class="w-4 h-4 ml-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20" fill="currentColor">
                <path fill="none" d="M0 0h24v24H0z"></path>
                <path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"></path>
              </svg>
            </a>
          </div>
        </div>

      </div>
    </div>

    <div class="w-full lg:w-5/6 lg:max-w-lg md:w-3/4">
      <img class="object-cover object-center rounded-lg " alt="${this.rhsImageAlt}" src="${this.rhsImageSrc}">
    </div>

  </div>
</section>
  `;
  }

  ///////////////////////////////////
  // Action button and description //
  ///////////////////////////////////

  protected ActionButtonAndDescriptionHTML() {
    return html`
<section class="${classMap(this.classes)}">

  <div class="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row lg:px-28">

    <div class="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-3/4 lg:pr-24 md:pr-16 md:mb-0">
      <h2 class="mb-8 text-xs font-semibold tracking-widest text-black title-font">${this.tagline}</h2>
      <h1 class="mb-8 text-2xl font-black tracking-tighter text-black md:text-5xl title-font">${this.title}</h1>
      <p class="mb-8 leading-relaxed text-left text-blueGray-600 ">${this.mainText}</p>

      <div class="flex flex-col justify-center lg:flex-row">
        <button @click="${this._handleActionClick}"
                class="action flex items-center px-6 py-2 mt-auto font-semibold text-white transition duration-500 ease-in-out transform bg-blue-600 rounded-lg hover:bg-blue-700 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">${this.actionButtonText}</button>
        <p class="mt-2 text-sm text-left text-blueGray-600 md:ml-6 md:mt-0">
          ${this.actionDescription}
          <br class="hidden lg:block"/>
          <a href="${this.actionDescriptionHref}" class="inline-flex items-center font-semibold text-blue-600 md:mb-2 lg:mb-0 hover:text-black "
             title="${this.actionDescriptionLinkTextTooltip}">
            ${this.actionDescriptionLinkText}
          </a>
        </p>

      </div>
    </div>

    <div class="w-full lg:w-5/6 lg:max-w-lg md:w-3/4">
      <img class="object-cover object-center rounded-lg " alt="${this.rhsImageAlt}" src="${this.rhsImageSrc}">
    </div>

  </div>

</section>
      `;
  }

  ///////////////
  // Rendering //
  ///////////////

  render() {
    switch (this.variant) {
      case LGLeftHeaderVariant.FourLogos: return this.fourLogosHTML();
      case LGLeftHeaderVariant.ThreeItemChecklist: return this.threeItemChecklistHTML();
      case LGLeftHeaderVariant.TwoCard: return this.twoCardHTML();
      case LGLeftHeaderVariant.EmailAndActionButton: return this.emailAndActionButtonHTML();
      case LGLeftHeaderVariant.ActionButtonAndDescription:
      default:
        return this.ActionButtonAndDescriptionHTML();
    }
  }

  private _handleActionClick() {
    const event = new Event("action-clicked", {bubbles: true, composed: true});
    this.dispatchEvent(event);
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "lg-left-header": LGLeftHeader
  }
}
