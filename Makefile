.PHONY: all clean \
				ensure-tool-yarn ensure-tool-npx ensure-component-env-var \
				target-dir print-package-filename \
				changelog package publish publish-prerelease \
				build build-tsc \
				serve serve-landing-page \
				serve-component-demo

VERSION ?= $(shell grep version package.json | cut -d ' ' -f 4 | tr -d ,\")

YARN ?= yarn
NPX ?= npx
GIT ?= git

all: build

clean:
	$(NPX) rimraf dist/*

###########
# Tooling #
###########

ensure-tool-yarn:
ifeq (,$(shell which $(YARN)))
	$(error "yarn binary not installed ($(YARN))")
endif

ensure-tool-npx:
ifeq (,$(shell which $(NPX)))
	$(error "npx binary not installed ($(NPX))")
endif

ensure-component-env-var:
ifeq (,$(COMPONENT))
	$(error "COMPONENT make/environment variable must be specified.")
endif

###############
# Development #
###############

COMPONENTS ?= lg-alert \
							lg-content \
							lg-footer \
							lg-left-header \
							lg-modal \
							lg-navbar \
							lg-sidebar-container

build: ensure-tool-yarn build-css
	$(YARN) build

build-tsc: ensure-tool-yarn
	$(YARN) build-tsc

build-tsc-watch: ensure-tool-yarn
	$(YARN) build-tsc-watch

build-css: ensure-tool-npx
	@$(foreach component, \
		$(COMPONENTS), \
		$(NPX) tailwindcss \
			--jit \
			--minify \
			--purge src/components/$(component)/index.ts \
			--config src/components/$(component)/tailwind.config.js \
			--output src/components/$(component)/tailwind-styles.generated.css; \
	)

serve: serve-landing-page

serve-component-demo: ensure-component-env-var
	$(YARN) serve src/components/$(COMPONENT)/demo.html

serve-landing-page:
	$(YARN) serve src/landing-page/index.html

#############
# Packaging #
#############

PACKAGE_NAME ?= landing-gear
PACKAGE_FILENAME ?= $(PACKAGE_NAME)-v$(VERSION).tgz
TARGET_DIR ?= dist
PACKAGE_PATH ?= $(TARGET_DIR)/$(PACKAGE_FILENAME)
CHANGELOG_FILE_PATH ?= CHANGELOG

target-dir:
	mkdir -p $(TARGET_DIR)

print-package-filename:
	@echo "$(PACKAGE_FILENAME)"

changelog:
	$(GIT) cliff --unreleased --tag=$(VERSION) --prepend=$(CHANGELOG_FILE_PATH)

# NOTE: if you try to test this package locally (ex. using `yarn add path/to/scout-apm-<version>.tgz`),
# you will have to `yarn cache clean` between every update.
# as one command: `yarn cache clean && yarn remove scout-apm && yarn add path/to/scout-apm-v0.1.0.tgz`
package: all target-dir
	$(YARN) pack
	mv $(PACKAGE_FILENAME) $(TARGET_DIR)/

publish: package
	$(YARN) publish \
		--tag latest \
		--new-version $(VERSION) \
		$(PACKAGE_PATH)

publish-prerelease: package
	$(YARN) publish \
		--tag pre \
		--prerelease \
		$(PACKAGE_PATH)
