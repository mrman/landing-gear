# Landing Gear #

Landing Gear is a set of [Lit][lit]-powered components that builds on the work done by [wickedtemplates][wickedtemplates] to create [wickedblocks][wickedblocks].

It was originally called "Wicked Landing Gear", but the "Wicked" was removed to avoid confusion as this code is not created or maintained by wickedtemplates.

# Getting started #

## 1. Use a smart bundler like [Parcel][parcel]

## 2. Install `landing-gear` via /YARN

```console
$ yarn add landing-gear
```

If you use `npm` then this would be:

```console
$ npm install landing-gear
```

## 3. Add HTML that uses one or more component imports

Use HTML like the folloing:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title>Your Site</title>
    <script type="module" src="../node_modules/landing-gear/dist/lg-alert/index.js"></script>
  </head>
  <body>
    <h1>Your Site</h1>
    <h2>A subtitle for your site</h2>

    <!-- Basic alert, powered by landing-gear -->
    <lg-alert class="alert"
              variant="info"
              title="Basic Alert"
              description="Important alert text that needs to be shown"></lg-alert>

  </body>
</html>
```

## Build your project

Build your project however it should be built depending on which bundler you used. If you used `parcel` this is as simple as:

```console
$ parcel
```

# Contributing #

## Adding a new component

To add a new component:

1. `yarn create @vitejs/app new-component --template lit-element-ts`
2. Copy over a `Makefile` from an existing component
3. Update the `package.json` with the project name, and appropriate `script` tasks and `devDependencies` to match other projects
4. Update `package.json` for use with [Vite library mode][vite-library-mode]

Alternatively you can also also copy an existing existing library, and make the appropriate replacements

## Viewing an existing component's demo page

To view an existing component's demo page, run the following:

```console
$ COMPONENT=lg-content make serve-component-demo
```

# TODO #

Here are some things that should really get done at some point:

- [ ] Setup [Parcel aliasing][parcel-aliasing] to support separate [Lit static build][lit-static-build-issue] from components (single `lit` import, multiple components, rather than every component having an instance of lit inside)

[parcel]: https://parceljs.org
[parcel-aliasing]: https://parceljs.org/features/dependency-resolution/#aliases
[lit-static-build-issue]: https://github.com/lit/lit/issues/2272

[lit]: https://lit.dev
[wickedtemplates]: https://wickedtemplates.com
[wickedblocks]: https://blocks.wickedtemplates.com/grids
[vite-library-mode]: https://vitejs.dev/guide/build.html#library-mode
